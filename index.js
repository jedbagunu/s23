console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		kanto: ["Brock", "Misty"]
	}/*,
	talk: function(){
		console.log("Pikachu I choose you")*/
	}

console.log(trainer)
console.log('Result of the dot notation:')
console.log(trainer.name)
console.log('Result of the bracket notation: ')
console.log(trainer.pokemon)



function Pokemon(name, level, health, attack){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.talk = function() {
		console.log( trainer.pokemon[0] +' I choosed You!')
	}

	this.tackle = function(target){
		let targetHealth = target.health -this.attack
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));

		if(target.health <= 0){
			target.faint()
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}


let pikachu = new Pokemon("Pikachu", 16);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);
pikachu.talk()
console.log(pikachu)
console.log(Geodude)
console.log(Mewtwo)
 
console.log("Round 1: FIGHT!")
Geodude.tackle(pikachu)
Geodude.faint()

console.log("Round 2: FIGHT!")
Mewtwo.tackle(Geodude)
Geodude.faint()



